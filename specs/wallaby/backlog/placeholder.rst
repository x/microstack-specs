..
  Copyright 2021 Canonical Ltd

  This work is licensed under a Creative Commons Attribution 3.0
  Unported License.
  http://creativecommons.org/licenses/by/3.0/legalcode

..
  This template should be in ReSTructured text. Please do not delete
  any of the sections in this template.  If you have nothing to say
  for a whole section, just write: "None". For help with syntax, see
  http://sphinx-doc.org/rest.html To test out your formatting, see
  http://www.tele3.cz/jbar/rest/rest.html

===========
Placeholder
===========

This file is only here for the initial repo commit to exercies tests while
there are no specs.

Problem Description
===================

Proposed Change
===============

Alternatives
------------

Implementation
==============

Assignee(s)
-----------

Gerrit Topic
------------

Work Items
----------

Repositories
------------

Documentation
-------------

Security
--------

Testing
-------

Dependencies
============
