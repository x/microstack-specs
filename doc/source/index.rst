==================================
OpenStack Microstack Project Plans
==================================

Specifications
==============

Here you can find the specs, and spec template, for each release:

.. toctree::
   :glob:
   :maxdepth: 1

   specs/wallaby/index

Process
=======

Documentation for microstack-specs process:

.. toctree::
   :maxdepth: 1

   How to submit a spec <readme>

For more details, look at spec template for the specific release.

Indices and tables
==================

* :ref:`search`
